@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                @if (count($errors) > 0)
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-12">
                    <h1 class="page-header">
                        Forms
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Directors
                        </li>
                    </ol>

                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    @if(isset($user))

                        {!! Form::model($user, ['url' => '/directors/'.$user->id, 'method'=>'patch'])  !!}
                    @else
                        {{Form::open(['route' => 'directors.store'])}}
                    @endif

                    <div class="form-group">
                        {{ Form::label('Name') }}
                        {!! Form::text('name', null,
                        array('class'=>'form-control','placeholder'=>'Directors full  name*')) !!}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Title') }}
                        {!! Form::text('title', null,
                        array('class'=>'form-control','placeholder'=>'Directors title*')) !!}
                    </div>


                    <div class="form-group">
                        {{ Form::label('Description') }}
                        {!! Form::textArea('description', null,
                        array('class'=>'form-control','placeholder'=>'Directors description i.e past achievements*')) !!}
                    </div>

                    <div class="form-group">
                        {{      Form::submit( 'Submit!',array('class'=>"btn btn-success"))}}
                    </div>

                        {!! Form::close()  !!}


                </div>
            </div>


            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>


    <!-- /#page-wrapper -->

@endsection