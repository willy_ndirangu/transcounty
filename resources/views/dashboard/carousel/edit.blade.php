@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                @if (count($errors) > 0)
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-12">
                    <h1 class="page-header">
                        Carousel
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Carousel
                        </li>
                    </ol>

                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    @if(isset($carousel))

                        {!! Form::model($carousel, ['url' => '/carousel/'.$carousel->id, 'method'=>'patch','files' => true])  !!}
                    @else
                        {{Form::open(['route' => 'carousel.store','files' => true])}}
                    @endif

                    <div class="form-group" style="padding-bottom: 5%">
                        {{ Form::label('Title') }}
                        {!! Form::text('title', null,
                        array('class'=>'form-control','placeholder'=>'e.g 50 x 100 plots')) !!}
                    </div>

                    <div class="form-group" style="padding-bottom: 5%">
                        {{ Form::label('Description') }}
                        {!! Form::text('description', null,
                        array('class'=>'form-control','placeholder'=>'Located 3km from Longonot township and 400m from the highway')) !!}
                    </div>


                    <div class="form-group" style="padding-bottom: 5%">
                        {{ Form::label('Image') }}
                        {!!Form::file('photo',
                        array('class'=>'form-control','placeholder'=>'land image')) !!}
                    </div>

                    <div class="form-group" style="padding-bottom: 15%">
                        {{      Form::submit( 'Submit!',array('class'=>"btn btn-success"))}}
                    </div>

                    {!! Form::close()  !!}


                </div>
            </div>


            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>


    <!-- /#page-wrapper -->

@endsection

@section('script')

    <script src="/js/tmce/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
@endsection