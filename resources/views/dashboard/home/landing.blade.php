@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->

                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Dashboard
                                <small> Overview</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li class="active">
                                    <i class="fa fa-dashboard"></i> Dashboard
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="wrapper wrapper-content animated fadeInRight">

                                    <div class="ibox-content m-b-sm border-bottom">
                                        <div class="p-xs">
                                            <div class="pull-left m-r-md">
                                                <i class="fa fa-globe text-navy mid-icon"></i>
                                            </div>
                                            <h2>Welcome to our dashboard</h2>
                                            <span>This is a  guide to help you navigate the system</span>
                                        </div>
                                    </div>

                                    <div class="ibox-content forum-container">

                                        <div class="forum-title">
                                            <div class="pull-right forum-desc">
                                            </div>
                                            <h3>General subjects</h3>
                                        </div>

                                        <div class="forum-item active">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="forum-icon">
                                                        <i class="fa fa-home"></i>
                                                    </div>
                                                    <a href="{{route('properties.index')}}" class="forum-item-title">Properties</a>
                                                    <div class="forum-sub-title">This section allows one to add,delete
                                                        or edit a property that is displayed in the main page.
                                                        Only the properties marked as available are shown.while adding a
                                                        property, an optional field with the image is included thus any
                                                        image that needs to be attached can be provided.
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="forum-item">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="forum-icon">
                                                        <i class="fa fa-users"></i>
                                                    </div>
                                                    <a href="{{route('directors.index')}}" class="forum-item-title">Directors</a>
                                                    <div class="forum-sub-title">The company directors and management
                                                        can be added from this section with their titles and
                                                        descriptions. Similarly any modifications to the profiles can be
                                                        done while in the section.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="forum-item active">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="forum-icon">
                                                        <i class="fa fa-comments-o"></i>
                                                    </div>
                                                    <a href="{{route('dashboard_contacts')}}" class="forum-item-title">Messages</a>
                                                    <div class="forum-sub-title">This section contains the messages left
                                                        in the contact section of the main site.
                                                        All the messages can be viewed and edited on this particular
                                                        section
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="forum-item">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="forum-icon">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                    <a href="{{route('company.index')}}" class="forum-item-title">Company</a>
                                                    <div class="forum-sub-title">This section allows one to edit and add
                                                        the company vision, mission and values.
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="forum-item active">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="forum-icon">
                                                        <i class="fa fa-picture-o"></i>
                                                    </div>
                                                    <a href="{{route('carousel.index')}}" class="forum-item-title">Carousel</a>
                                                    <div class="forum-sub-title">This section provides the user with the
                                                        ability to change the main site images that rotate on the
                                                        landing page together with the information displayed alogside
                                                        those images.Images can be added,deleted or the information
                                                        accompanying them changed.
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                        <div class="forum-title">

                                            <h3>Other subjects</h3>
                                        </div>

                                        <div class="forum-item">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="forum-icon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                    <a href="{{route('logout')}}" class="forum-item-title">Logout </a>
                                                    <div class="forum-sub-title">at the far right corner the name of the
                                                        logged in user is display with a down caret where on click a
                                                        logout option is provided. this can be used to logout the user.
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection