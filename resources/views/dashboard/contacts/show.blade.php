@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">
            @if(Session::has('message'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            <ul>

                                <li>{{ Session::get('message') }}</li>

                            </ul>
                        </div>
                    </div>
                </div>
        @endif
        <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Directors
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>

                        <li class="active">
                            Directors
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <div class="container">
                <div class="row">
                    <table id="mytable" class="table  table-striped" >

                        <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Delete</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td>{{$message->name}}</td>
                                <td>{{$message->email}}</td>
                                <td>{{$message->phone_number}}</td>
                                <td>{{$message->subject}}</td>
                                <td>{{$message->message}}</td>
                                <td>
                                    <p data-placement="top" data-toggle="tooltip" title="Delete">
                                        <button
                                                id="{{$message->id}}"
                                                name="{{$message->name}}" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal"
                                                data-target="#delete-modal"><span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </p>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>


@endsection

<!--modal-->
<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>
            <div class="modal-body edit-content">
                ...
            </div>
            <div class="modal-footer">

                    <button type="button" style="width: 20%" class="btn btn-success vertical-align"
                            data-dismiss="modal">No
                    </button>
                <a id='form_delete' >  <button type="submit" style="width: 20%" class="btn btn-danger">Yes</button></a>



            </div>
        </div>
    </div>
</div>
<!--modal-->
@section('script')
    <script>
        $('#delete-modal').on('show.bs.modal', function (e) {

            var $modal = $(this),
                    esseyId = e.relatedTarget.id;
            name = e.relatedTarget.name;

            $modal.find('.edit-content').html("Are you sure you want to delete " + name + 'message?');
            $modal.find('#form_delete').attr("href", "/dashboard/contacts/delete/" + esseyId);


        })
    </script>
@endsection