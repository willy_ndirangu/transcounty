@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                @if (count($errors) > 0)
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-12">
                    <h1 class="page-header">
                        Properties
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Properties
                        </li>
                    </ol>

                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    @if(isset($company))

                        {!! Form::model($company, ['url' => '/company/'.$company->id, 'method'=>'patch'])  !!}
                    @else
                        {{Form::open(['route' => 'company.store','files' => true])}}
                    @endif

                    <div class="form-group">
                        {{ Form::label('Title') }}
                        {!! Form::text('title', null,
                        array('class'=>'form-control','placeholder'=>'Mission')) !!}
                    </div>
                        <div class="form-group">
                            {{ Form::label('Name') }}
                            {!! Form::text('contains', null,
                            array('class'=>'form-control','placeholder'=>'Integrity')) !!}
                        </div>
                    <div class="form-group">
                        {{ Form::label('Description') }}
                        {!! Form::textArea('particulars', null,
                        array('class'=>'form-control','placeholder'=>'a little description of the Company attribute*')) !!}
                    </div>



                    <div class="form-group">
                        {{      Form::submit( 'Submit!',array('class'=>"btn btn-success"))}}
                    </div>

                    {!! Form::close()  !!}


                </div>
            </div>


            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>


    <!-- /#page-wrapper -->

@endsection

@section('script')

    <script src="/js/tmce/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
@endsection