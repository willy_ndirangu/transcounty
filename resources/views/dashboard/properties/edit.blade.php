@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                @if (count($errors) > 0)
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-12">
                    <h1 class="page-header">
                        Properties
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>
                        <li class="active">
                            Properties
                        </li>
                    </ol>

                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    @if(isset($property))

                        {!! Form::model($property, ['url' => '/properties/'.$property->id, 'method'=>'patch','files' => true])  !!}
                    @else
                        {{Form::open(['route' => 'properties.store','files' => true])}}
                    @endif

                    <div class="form-group">
                        {{ Form::label('Title') }}
                        {!! Form::text('title', null,
                        array('class'=>'form-control','placeholder'=>'e.g 1/8 plots located in')) !!}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Description') }}
                        {!! Form::textArea('description', null,
                        array('class'=>'form-control','placeholder'=>'a little description of the property*')) !!}
                    </div>


                    <div class="form-group">
                        {{ Form::label('Features and Amenities') }}
                        {!! Form::textArea('features', null,
                        array('class'=>'form-control','placeholder'=>'e.g ready title deeds')) !!}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Available') }}
                        {!! Form::checkBox('available', true,
                        array('class'=>'form-control')) !!}
                    </div>

                    <div class="form-group">
                        {{ Form::label('Percentage sold') }}
                        {!! Form::number('percentage_sold', null,
                        array('class'=>'form-control','placeholder'=>'e.g 10')) !!}
                    </div>

                    <div class="form-group">
                        {{ Form::label('Image') }}
                        {!!Form::file('photo',
                        array('class'=>'form-control','placeholder'=>'land image')) !!}
                    </div>

                    <div class="form-group">
                        {{      Form::submit( 'Submit!',array('class'=>"btn btn-success"))}}
                    </div>

                    {!! Form::close()  !!}


                </div>
            </div>


            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>


    <!-- /#page-wrapper -->

@endsection

@section('script')

    <script src="/js/tmce/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
@endsection