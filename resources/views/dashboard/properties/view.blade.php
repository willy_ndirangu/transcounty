@extends('dashboard')
@section('content')

    <div id="page-wrapper">

        <div class="container-fluid">
            @if(Session::has('message'))
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            <ul>

                                <li>{{ Session::get('message') }}</li>

                            </ul>
                        </div>
                    </div>
                </div>
        @endif
        <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Properties
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i> <a href="{{route('dashboard')}}">Dashboard</a>
                        </li>

                        <li class="active">
                            Properties
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">

                    <a href="{{ route('properties.create') }}">
                        <button type="submit" class="btn btn-success">Add Property</button>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-block">
                            <h3 class="card-title">  {{$property->title}}</h3>
                            @if($property->image)
                                <img src="{{route('images',$property->image)}}" alt="Card image cap" height="70%"
                                     width="100%">
                            @endif
                            <h3 class="card-subtitle mb-2">Description</h3>
                            <p class="card-text">{!! $property->description !!}</p>
                            <h4 class="card-subtitle mb-2">Features and Amenities</h4>
                            <p class="card-text" style="font-size: 14px">{!! $property->features !!}</p>
                            <h5 class="card-subtitle mb-2 " style="color: green">Percentage
                                Sold: {!! $property->percentage_sold !!}%</h5>
                            <h5 class="card-subtitle mb-2" style="color: green">Available: @if( $property->available )
                                    Yes
                                @else
                                    No
                                @endif</h5>

                            </i> <a href="{{route('properties.edit',$property->id)}}" class="card-link center"
                                    data-toggle="tooltip" title="Edit" style="color: #00a65a"><i
                                        class="fa fa-pencil-square-o fa-2x"> &nbsp;</i></a>
                            <a href="" data-toggle="modal" data-target="#delete-modal"
                               class="card-link delete_director "
                               id="{{$property->id}}"
                               name="{{$property->title}}"
                               data-toggle="tooltip" title="Delete" style="color: red"><i class="fa fa-trash fa-2x"
                                                                                          aria-hidden="true"></i>
                            </a>
                            <hr>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection

<!--modal-->
<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>
            <div class="modal-body edit-content">
                ...
            </div>
            <div class="modal-footer">
                <form id="form_delete" method="POST">
                    <input type="hidden" name="_method" value="DELETE"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="button" style="width: 20%" class="btn btn-success vertical-align"
                            data-dismiss="modal">No
                    </button>
                    <button type="submit" style="width: 20%" class="btn btn-danger">Yes</button>

                </form>

            </div>
        </div>
    </div>
</div>
<!--modal-->
@section('script')
    <script>
        $('#delete-modal').on('show.bs.modal', function (e) {

            var $modal = $(this),
                    esseyId = e.relatedTarget.id;
            name = e.relatedTarget.name;

            $modal.find('.edit-content').html("Are you sure you want to delete " + name + '?');
            $modal.find('#form_delete').attr("action", "/properties/" + esseyId);


        })
    </script>
@endsection