@extends('auth')

@section('content')
    <h1 class="text-center login-title">Reset password</h1>
    <div class="account-wall">

            <!---errors--->
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        {{--@if(session('status'))--}}

            {{--{!! flash()->overlay("Reset","Check your email for the password reset link") !!}--}}
        {{--@endif--}}

        <!---errors--->
            <!---login form---->
            {!! Form::open(array('route' => 'email_password_reset_link', 'class' => 'form form-signin')) !!}

            {!! csrf_field() !!}
            <div class="row column log-in-form ">

                <h1 class="text-center login-title">Enter email to send password reset link</h1>

                {!! Form::text('email', null,
                    array('required',
                          'class'=>'form-control',
                          'placeholder'=>'Your e-mail address *')) !!}




                {!! Form::submit('Send the link',
                  array('class'=>'btn btn-lg btn-primary btn-block ')) !!}


            </div>
            {!! Form::close() !!}

        </div>
    </div>
@stop
