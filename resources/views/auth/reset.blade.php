@extends('auth')

@section('content')


    <!---errors--->
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>

    <h1 class="text-center login-title">Reset Password</h1>
    <div class="account-wall">
        <!---errors--->
        <!---login form---->
        {!! Form::open(array('route' => 'reset', 'class' => 'form form-signin')) !!}
        {!! csrf_field() !!}
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="row column log-in-form ">

            <h4 class="text-center">Log in with you email account</h4>


            {!! Form::label('Your E-mail Address') !!}
            {!! Form::text('email',null,
                array('required',
                      'class'=>'form-control',
                      'placeholder'=>'Your e-mail address *')) !!}


            {!! Form::label('Password') !!}
            {!! Form::password('password',
                array('required',
                      'class'=>'form-control')) !!}

            {!! Form::label('Password') !!}
            {!! Form::password('password_confirmation',
                array('required',
                      'class'=>'form-control')) !!}





            {!! Form::submit('Reset',
              array('class'=>'btn btn-lg btn-primary btn-block ')) !!}


        </div>
        {!! Form::close() !!}

    </div>

@stop
