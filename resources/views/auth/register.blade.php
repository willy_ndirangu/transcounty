@extends('auth')

@section('content')


    <!---errors--->
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>



    <h1 class="text-center login-title">Sign in to continue to Transcounty Dashboard</h1>
    <div class="account-wall">
        <!---errors--->
        <!---login form---->
        {!! Form::open(array('route' => 'register', 'class' => 'form form-signin')) !!}

        {!! csrf_field() !!}
        <div class="row column log-in-form ">

            <h4 class="text-center">Register for an account with easyboard</h4>

            {!! Form::label('Full Name') !!}
            {!! Form::text('name', null,
                array('required',
                      'class'=>'form-control',
                      'placeholder'=>'Your full name *')) !!}




            {!! Form::label('Your E-mail Address') !!}
            {!! Form::text('email', null,
                array('required',
                      'class'=>'form-control',
                      'placeholder'=>'Your e-mail address *')) !!}


            {!! Form::label('Password') !!}
            {!! Form::password('password',
                array('required',
                      'class'=>'form-control',
                      'placeholder'=>'Password *')) !!}

            {!! Form::label('Confirm Password') !!}
            {!! Form::password('password_confirmation',
                array('required',
                      'class'=>'form-control',
                      'placeholder'=>'confirm password')) !!}



            {!! Form::submit('Register',
              array('class'=>'btn btn-lg btn-primary btn-block')) !!}


        </div>
        {!! Form::close() !!}
    </div>

@stop
