@extends('auth')

@section('content')

    @if(Auth::check())
        {!! Auth::logout() !!}
    @endif

    {{--<div class="row">--}}
    {{--<div class="medium-6 medium-centered large-6 large-centered columns">--}}
    <!---errors--->
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>

    <h1 class="text-center login-title">Sign in to continue to Transcounty Dashboard</h1>
    <div class="account-wall">
        <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
             alt="">
        <!---errors--->
        <!---login form---->
        {!! Form::open(array('route' => 'login', 'class' => 'form form-signin')) !!}
        {!! csrf_field() !!}
        {{--<div class="row column log-in-form ">--}}

        {{--<h4 class="text-center">Log in with you email account</h4>--}}


        {{--{!! Form::label('Your E-mail Address',null,array('class'=>'checkbox pull-left')) !!}--}}
        {!! Form::text('email', null,
            array('required',
                  'class'=>'form-control',
                  'placeholder'=>'e-mail address *')) !!}


        {{--{!! Form::label('Password',null,array('class'=>'checkbox pull-left')) !!}--}}
        {!! Form::password('password',
            array('required',
                  'class'=>'form-control',
                  'placeholder'=>'Password *')) !!}





        {!! Form::submit('login',
          array('class'=>'btn btn-lg btn-primary btn-block')) !!}


        <p class="text-center"><a href="/password/email">Forgot your password?</a></p>


        {{--</div>--}}
        {!! Form::close() !!}
    </div>

    {{--</div>--}}
    {{--</div>--}}
@stop