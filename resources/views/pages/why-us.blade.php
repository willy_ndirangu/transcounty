@extends('app')

@section('alert')
<p>
  Why us?
</p>
@stop
@section('content')
<!---image--->



<div class="row">

     <img src="/img/Why-us.jpg" class="img-responsive" alt="Cinque Terre" >


</div>

<div class="row">
  <div class="col-md-12 why_us_large">
    <p>
      TCICL capabilities in real estate investments  are unrivalled due to the following reasons:
    </p>

  </div>

</div>
<div class="row">
  <div class="col-md-12 why_us_small">
    @foreach ($reasons as $reason)
    <p class="blocktext">
     {{$reason->id}}.  {{$reason->particulars}}.
    </p>
    @endforeach

  </div>

</div>


<!---image--->

@stop
