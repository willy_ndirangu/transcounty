<!--
@Author: Wilson Ndirangu <wndirangu>
@Date:   2016-06-25T21:53:42+03:00
@Email:  wnwillyndirangu@gmail.com
@Last modified by:   wndirangu
@Last modified time: 2016-08-11T19:14:38+03:00
@License: MIT
-->


@extends('app')

@section('alert')
    <p>
        Properties.
    </p>
@stop

@section('content')
    @foreach($properties as $property)
        @if($property->available)

            <div class="row" id="yellow">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-warning coupon">
                        <div class="panel-heading" id="head">
                            <div class="panel-title" id="title">

                                <span class="hidden-xs ">{!! $property->title !!} </span>
                                <span class="visible-xs">{!! $property->title !!} </span>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if($property->image)
                                <img src="{{route('images',$property->image)}}" class="coupon-img img-rounded">
                            @endif
                            <div class="col-md-9 about_text">

                                    {!! $property->description !!}



                                <div class="row">

                                    <ul class="items about_text col-md-12  col-md-offset-4 center ">
                                        <h2>Features & Amenities</h2>
                                        {!! $property->features !!}

                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-3 text-warning">
                                <div class="offer">
                                    <span class="usd"><sup></sup></span>
                                    <span class="number">

                            @if($property->percentage_sold)
                                            {{$property->percentage_sold}}% Sold
                                        @endif
                            </span>
                                    <span class="cents"><sup></sup></span>
                                </div>
                            </div>
                            <div class="col-md-12">

                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="coupon-code">

                            </div>

                            <div class="exp">Available</div>

                        </div>
                    </div>
                </div>
            </div>
            <hr>
        @endif
    @endforeach
    {{--<div class="row" id="yellow">--}}
        {{--<div class="col-md-10 col-md-offset-1">--}}
            {{--<div class="panel panel-warning coupon">--}}
                {{--<div class="panel-heading" id="head">--}}
                    {{--<div class="panel-title" id="title">--}}

                        {{--<span class="hidden-xs "> Longonot View, 50 x 100 Plots, Nakuru County </span>--}}
                        {{--<span class="visible-xs">Longonot View, 50 x 100 Plots, Nakuru County</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<img src="/img/transcounty_map-compressor.png" class="coupon-img img-rounded">--}}
                    {{--<div class="col-md-9">--}}
                        {{--<p class="about_text">--}}
                            {{--Located 3km from Longonot township and 400m from the highway.--}}

                        {{--</p>--}}
                        {{--<div class="row">--}}
                            {{--<h2>Features & Amenities</h3>--}}
                                {{--<ul class="items about_text">--}}
                                    {{--<li>Ready title deeds</li>--}}

                                    {{--<li> Spectacular view of L. Naivasha and Mt Longonot</li>--}}

                                    {{--<li> Overlooks the upcoming Longonot Gate developments and proposed Naivasha--}}
                                        {{--industrial park/SGR terminal--}}
                                    {{--</li>--}}

                                {{--</ul>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="col-md-3 text-warning">--}}
                        {{--<div class="offer">--}}
                            {{--<span class="usd"><sup> </sup></span>--}}
                            {{--<span class="number"><p class="about_text">--}}
                             {{--Call <br>--}}
                             {{--0739-255-945--}}
                             {{--<br>or <br>--}}
                             {{--0724-625-444--}}
                           {{--</p>  </span>--}}
                            {{--<span class="cents"><sup>  </sup></span>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-12">--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-footer">--}}
                    {{--<div class="coupon-code">--}}

                    {{--</div>--}}
                    {{--<div class="exp">Available</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row" id="yellow">--}}
        {{--<div class="col-md-10 col-md-offset-1">--}}
            {{--<div class="panel panel-warning coupon">--}}
                {{--<div class="panel-heading" id="head">--}}
                    {{--<div class="panel-title" id="title">--}}

                        {{--<span class="hidden-xs "> 50 x 100  plots Koma Hill site. Machakos county  </span>--}}
                        {{--<span class="visible-xs"> 50 x 100  plots Koma Hill site. Machakos county </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<!-- <img src="/img/transcounty_map-compressor.png" class="coupon-img img-rounded"> -->--}}
                    {{--<div class="col-md-11">--}}
                        {{--<p class="about_text">--}}
                            {{--Located just 100meters off Kangudo road, overlooking Koma Hill shrine.--}}
                        {{--</p>--}}
                        {{--<div class="row">--}}
                            {{--<h3>Features & Amenities</h3>--}}
                            {{--<ul class="items about_text">--}}
                                {{--<li>Ready title deeds</li>--}}

                                {{--<li> Panoramic views mt. Kilimambogo</li>--}}

                            {{--</ul>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="col-md-1 text-warning">--}}
                        {{--<div class="offer">--}}
                            {{--<span class="usd"><sup> </sup></span>--}}
                            {{--<span class="number"><p class="about_text">--}}
                                {{--<!-- Call <br>--}}
                                {{--0739-255-945--}}
                                {{--<br>or <br>--}}
                                {{--0724-625-444--}}
                              {{--</p>  </span> -->--}}
                              {{--<span class="cents"><sup>  </sup></span>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-12">--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-footer">--}}
                    {{--<div class="coupon-code">--}}

                    {{--</div>--}}
                    {{--<div class="exp">Available</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<hr>--}}
    {{--<div class="row" id="yellow">--}}
        {{--<div class="col-md-10 col-md-offset-1">--}}
            {{--<div class="panel panel-warning coupon">--}}
                {{--<div class="panel-heading" id="head">--}}
                    {{--<div class="panel-title" id="title">--}}

                        {{--<span class="hidden-xs "> 1/8 plots Kamulu plots. Nairobi County  </span>--}}
                        {{--<span class="visible-xs"> 1/8 plots Kamulu plots. Nairobi County </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<!-- <img src="/img/transcounty_map-compressor.png" class="coupon-img img-rounded"> -->--}}
                    {{--<div class="col-md-9">--}}
                        {{--<p class="about_text">--}}
                            {{--1/8 plots on gently sloping land,overlooking Joska Town and adjacent to Athi City County--}}
                            {{--Primary., and only 500 meters--}}
                            {{--from proposed Greater Eastern bypass.--}}
                        {{--</p>--}}
                        {{--<div class="row">--}}

                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="col-md-1 text-warning">--}}
                        {{--<div class="offer">--}}
                            {{--<span class="usd"><sup></sup></span>--}}
                            {{--<span class="number">60% sold </span>--}}
                            {{--<span class="cents"><sup></sup></span>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-12">--}}

                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="panel-footer">--}}
                    {{--<div class="coupon-code">--}}

                    {{--</div>--}}
                    {{--<div class="exp">Available</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<hr>--}}

@stop
