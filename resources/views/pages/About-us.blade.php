<!--
@Author: Wilson Ndirangu <wndirangu>
@Date:   2016-06-25T21:53:42+03:00
@Email:  wnwillyndirangu@gmail.com
@Last modified by:   wndirangu
@Last modified time: 2016-08-24T10:11:13+03:00
@License: MIT
-->


@extends('app')

@section('alert')
    <p>
        About us.
    </p>
@stop
@section('content')
    <!---image--->



    <div class="row">

        <img src="/img/about-us.jpg" class="img-responsive" alt="Cinque Terre">


    </div>

    <div class="row">
        <div class="col-md-12 about_text">
            <p>
                Trans-county investment company (TCIC) ltd is a fully fledged real estate company registered in Kenya
                with a vission
                of empowering kenyans of all walks of life to own valuable homes and properties. At TCIC we always
                strive towards
                proven realty business expertise and experience by broadening our clients choices
                through a wide range of tailor-made investment opportunities. We are aware of constant changes in the
                industry and
                are committed to continuous research in order to provide sustainable and significant innovations.Our
                Strategy is
                basically to fulfill customers’ needs and wants and to enhance our employee satisfaction.We also ensure
                environmental
                sustainability through our greening policy.
            </p>


        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <p class="our_team">
                Our Team
            </p>
            <div class="container-fluid">
                <div class="row ">
                    @foreach($directors as $indexKey => $director)
                        @if($indexKey%2 !=0)
                            <div class="row">
                                @endif

                                <div class="col-md-6 about_text">
                                    <h3>{{$director->name}}</h3>
                                    <h3>{{$director->title}}</h3>
                                    <p class="about_text">
                                        {{$director->description}}
                                    </p>
                                </div>
                                @if($indexKey%2 !=0)
                            </div>
                        @endif
                    @endforeach


                </div>
            </div>

        </div>


    </div>


    <!---image--->

@stop
