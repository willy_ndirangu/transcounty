@extends('app')


@section('alert')
<p>
  Enquiries & comments
</p>
@stop
@section('content')
<!---after successful completion of ccontact form--->
@if( Session::has('message') )
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('message') }}
    </div>
@endif
<!---after successful completion of ccontact form--->
<!---Message at top of page--->
<div class="row">
  <div class="col-md-12 contact_text">
    <p>
      <h3>We’d Love to Hear from You!</h3>
      We're happy to answer any questions that you may have.please fill the form below and we
      will get back to you asap.

    </p>

  </div>

</div>

<!---Message at top of page--->
<!---errors--->
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
<!---errors--->
<!---alert---->
{!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}
<div class="container">


<div class="col-md-6">


<div class="form-group">
    {!! Form::label('Your Name') !!}
    {!! Form::text('name', null,
        array('required',
              'class'=>'form-control',
              'placeholder'=>'Your name*')) !!}
</div>

<div class="form-group">
    {!! Form::label('Your E-mail Address') !!}
    {!! Form::text('email', null,
        array('required',
              'class'=>'form-control',
              'placeholder'=>'Your e-mail address *')) !!}
</div>
<div class="form-group">
    {!! Form::label('Mailing address') !!}
    {!! Form::text('mailing', null,
        array(
              'class'=>'form-control',
              'placeholder'=>'P.O BOX ..')) !!}
</div>
<div class="form-group">
    {!! Form::label('Mobile Number') !!}
    {!! Form::text('phone_number', null,
        array(
              'class'=>'form-control',
              'placeholder'=>'+254..*')) !!}
</div>

<div class="form-group">
    {!! Form::label('Inquiry subject') !!}
    {!! Form::text('subject', null,
        array('required',
              'class'=>'form-control',
              'placeholder'=>'Subject*')) !!}
</div>

<div class="form-group">
    {!! Form::label('Your Message') !!}
    {!! Form::textarea('message', null,
        array('required',
              'class'=>'form-control',
              'placeholder'=>'Your message *')) !!}
</div>

<div class="form-group">
    {!! Form::submit('Contact Us!',
      array('class'=>'btn btn-primary')) !!}
</div>
</div>
</div>
{!! Form::close() !!}

@stop
