<!--
@Author: Wilson Ndirangu <wndirangu>
@Date:   2016-06-25T21:53:42+03:00
@Email:  wnwillyndirangu@gmail.com
@Last modified by:   wndirangu
@Last modified time: 2016-08-24T16:11:41+03:00
@License: MIT
-->


@extends('app')
@section('header')
@stop

@section('alert')
    <p>Leading the way in provision of prime, secure and affordable properties to the society . </p>
@stop
@section('content')
    <!---alert---->


    <!---alert---->

    <!---carousel---->


    <div class="row">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($carousels as $index=> $carousel)
                    @if($index==0)
                        <div class="item active">
                            @else
                                <div class="item ">
                                    @endif
                                    <img src="{{route('images',$carousel->image)}}" alt="Flower">
                                    <div class="carousel-caption">
                                        <h3>{{$carousel->title}}</h3>
                                        <p>{{$carousel->description}}</p>
                                    </div>
                                </div>
                            @endforeach
                         
                            <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                        </div>
            </div>


            <!---carousel---->
            <!---company_details--->
            <div class="row">

                <div class="col-md-6">
                    @foreach($missions as $mission)

                        <h3>{{$mission->contains}}</h3>
                        .<p>
                            {!! $mission->particulars !!}
                        </p>
                    @endforeach
                </div>
                <div class="col-md-6">
                    <h2>Our values</h2>
                    @foreach ($values as $value)
                        <h3>{{$value->contains}}</h3>
                        .<p>
                            {!! $value->particulars !!}
                        </p>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
    <!---company_details--->
@stop
@section('footer')
@stop
