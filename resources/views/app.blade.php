<!--
@Author: Wilson Ndirangu <wndirangu>
@Date:   2016-06-25T21:53:42+03:00
@Email:  wnwillyndirangu@gmail.com
@Last modified by:   wndirangu
@Last modified time: 2016-08-11T19:23:59+03:00
@License: MIT
-->


<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transcounty Investments Company</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <!---font awesome --->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">


    <!-- Latest compiled JavaScript -->
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/app.js" type="text/javascript"></script>

    <!--- favicon--->
    <link href="/img/favicon.ico" rel="icon" type="image/x-icon"/>

</head>
<body>
<!--- Navigation bar--->
<div class="container-fluid">
    <div class="container">
        <div class="row">
            @yield('header')
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand " href="{{ URL::action('PagesController@index') }}"><img
                                    class="tcil_logo" src="/img/logo.jpg"/> </a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav  navbar-right">
                            <li class="{{ Request::is( '/') ? 'active' : '' }}"><a
                                        href="{{ URL::action('PagesController@index') }}"><span>Home</span></a></li>
                            <li class="{{ Request::is( 'About') ? 'active' : '' }}"><a
                                        href="{{ URL::action('PagesController@about') }}"><span>About us</span></a></li>
                            <li class="{{ Request::is( 'Properties') ? 'active' : '' }}"><a
                                        href="{{ action('PagesController@properties') }}"><span>Properties</span></a>
                            </li>
                            <li class="{{ Request::is( 'Why-us') ? 'active' : '' }}"><a
                                        href="{{ action('PagesController@why_us') }}"><span>Why us?</span></a></li>
                            <li class="{{ Request::is( 'Contacts') ? 'active' : '' }}"><a href="{{ route('contact') }}"><span>Contacts</span></a>
                            </li>
                        </ul>


                    </div>
                </div>
                <div class="row hidden-xs hidden-md-down">
                    <p>Real Estate dealers</p>
                </div>

            </nav>


            <!--- Navigation bar end--->
            <!---alert---->
            <div class="container">
                <div class="container-fluid">

                    <div class="row vertical-align hidden-md">
                        <div class="col-md-12 vcentre">
                            @yield('alert')

                        </div>
                    </div>
                </div>
            </div>

            @yield('content')


        <!---footer---->
            @yield('footer')


            <div class="container">
                <footer class="footer-distributed">
                    <div class="row">

                        <div class="footer-left col-md-4 ">

                            <h3>TCIC Ltd<span></span></h3>

                            <p class="footer-links">
                                <a href="{{ URL::action('PagesController@index') }}">Home</a>
                                ·
                                <a href="{{ URL::action('PagesController@about') }}">About-us</a>
                                ·
                                <a href="{{ URL::action('PagesController@properties') }}">Properties</a>
                                ·
                                <a href="{{ URL::action('PagesController@why_us') }}">Why-us?</a>
                                ·
                                <a href="{{ URL::action('PagesController@contacts') }}">Contacts</a>


                            </p>

                            <p class="footer-company-name">TCIC ltd &copy; {{Carbon\Carbon::now()->year}}</p>
                        </div>

                        <div class="footer-center col-md-4">
                            <div>
                                <p><span>      </span> Contacts</p>
                            </div>
                            <div>
                                <i class="fa fa-map-marker"></i>
                                <p><span></span>Kamulu, Kenya</p>
                            </div>

                            <div>
                                <i class="fa fa-phone"></i>
                                <p>0735940207/0739255945</p>
                            </div>

                            <div>
                                <i class="fa fa-envelope"></i>
                                <p style="color:#ffffff !important;">transcountyicl@gmail.com</p>
                            </div>

                        </div>

                        <div class="footer-right col-md-4">

                            <p class="footer-company-about">
                                <span>About the company</span>
                                TCICL is a leading real estate company in provision of prime, secure and affordable
                                properties to kenyans of all walks of life.
                            </p>

                            <div class="footer-icons">

                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-github"></i></a>

                            </div>

                        </div>
                    </div>
                </footer>

                <!---footer---->
            </div>
        </div>

    </div>


</div>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Course",
  "name": "Transcounty Investments LTD ",
  "description": "Properties",
  "provider": {
    "@type": "Organization",
    "name": "TCIC",
    "sameAs": "http://www.transcountyivestments.co.ke/Properties"
  }
}

</script>

</body>

</html>
