<!--
 * Cytonn Technologies
 * @author Ndirangu Wilson <wndirangu@gmail.com>
--->

<!doctype html>
<html ng-app="dashboard" lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Board Management Application</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/auth.css">


</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">



                <!-- Authentication content loading -->
                @yield('content')
            </div>
        </div>
    </div>
</div>


</body>
</html>