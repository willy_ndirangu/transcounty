<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('contains');
            $table->text('particulars');
            $table->timestamps();

        });
        $data=[
       [
           'title' => 'Mission',
           'contains' => 'Mission',
           'particulars' => "TCICL is committed to lead the way in provision of prime, secure and affordable properties to society."
       ],
       [
           'title' => 'TICL',
           'contains' => 'Who we are',
           'particulars' => "TCICL is a leading real estate company in provision of prime, secure and affordable properties to kenyans of all walks of life "
       ],
       [
         'title' => 'Vision',
       'contains' => 'Vision',
       'particulars' => "To empower Kenyans to acquire valuable homes and properties for posterity."
   ],

       [
           'title' => 'Values',
           'contains' => 'Integrity',
           'particulars' => "We will be guided by the highest standards of ethical business conduct and by this simple principle: Do the right thing. Our business is built on long-lasting relationships, founded on trust.
            Our handshake is our bond. We stand behind our promises. We treat our employees, customers and partners with fairness, honesty and respect, just as we would want them to treat us.
            At TICL, our goal is to build long-term relationships and trusted partnerships. "
       ],
       [
           'title' => 'Values',
           'contains' => 'Customer focus',
           'particulars' => "We live by and for our customers success, we want to be their top-of-mind and top-of-heart partners.
           We build lasting relationships with our clients and associates and  strive to exceed their expectations in affordability, quality and value derived from our properties."
       ],
       [
           'title' => 'Values',
           'contains' => 'Consistency',
           'particulars' => "We will consistently treat customers and company resources with the respect they deserve."
       ],
       [
           'title' => 'Values',
           'contains' => 'Accountability',
           'particulars' => "We are each personally accountable for the highest standards of behavior,
           including honesty and fairness in all aspects of our work."
       ],
       [
           'title' => 'Values',
           'contains' => 'Professionalism',
           'particulars' => " We exercise high levels of professionalism in our work and  use the most appropriate skills and competencies,
           working collaboratively towards our common goal of providing valuable homes and properties. "
       ]
     ];

        App\Company::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company');
    }
}
