<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhyUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Why_us', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('contains');
            $table->text('particulars');
        });
        $data=[
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We add value to our properties"
       ],
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We  ensure environmental sustainability through our greening policy"
       ],
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We are committed to our customers and this ensures we offer excellent customer service"
       ],
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We pay our taxes"
       ],
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We  ensure prompt title deed production"
       ],
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We conduct CSR activities"
       ],
       [
           'title' => 'why us',
           'contains' => 'reason',
           'particulars' => "We create job opportunities"
       ]

     ];
     App\why_us::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Why_us');
    }
}
