<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Director extends Eloquent
{

    protected $table = "directors";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'description',
    ];


}