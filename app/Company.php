<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Company extends Eloquent
{

    protected $table = "company";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contains', 'title', 'particulars'
    ];

}
