<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Carousel extends Eloquent
{

    protected $table = "carousel";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'title', 'description'
    ];

}
