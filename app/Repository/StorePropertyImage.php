<?php

 namespace App\Repository;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\File;

class StorePropertyImage
{

    public function __construct( UploadedFile $file)
    {
        $this->file=$file;
    }
    /*
     * function  to name a file
     */
    public function makeFileName( )
    {
        $name= sha1(
            time() .$this->file->getClientOriginalname()
        );
        $extension =$this->file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

    /*
     * returns the storage path for a given file
     */
    public function storagePath()
    {

        return 'lands/';

    }

    /*
     * stores a file
     */
    public function store()
    {
        Storage::put($this->storagePath().$this->makeFileName(), File::get(($this->file)));
        return $this->makeFileName();
    }

}