<?php
namespace App\Repository;


use App\Carousel;
use App\Http\Requests\Request;
use App\Property;

class CarouselRepository
{
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) {
            $image = new StorePropertyImage($request->file('photo'));
            $image_path = $image->store();
            $request['image'] = $image_path;

        }

        Carousel::create($request->all());

    }

    public function update(Request $request,$id)
    {
        $carousel= Carousel::findOrFail($id);
        if ($request->hasFile('photo')) {
            $image = new StorePropertyImage($request->file('photo'));
            $image_path = $image->store();
            $request['image'] = $image_path;

        }

        $carousel->update($request->all());

    }


}