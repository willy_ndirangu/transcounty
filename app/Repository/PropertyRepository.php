<?php
namespace App\Repository;


use App\Http\Requests\Request;
use App\Property;

class PropertyRepository
{
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) {
            $image = new StorePropertyImage($request->file('photo'));
            $image_path = $image->store();
            $request['image'] = $image_path;

        }

        Property::create($request->all());

    }

    public function update(Request $request,$id)
    {
        $property= Property::findOrFail($id);
        if ($request->hasFile('photo')) {
            $image = new StorePropertyImage($request->file('photo'));
            $image_path = $image->store();
            $request['image'] = $image_path;

        }

        $property->update($request->all());

    }


}