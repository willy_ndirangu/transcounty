<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Property extends Eloquent{

    protected $table="properties";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'features', 'title', 'description','available','image','percentage_sold'
    ];

}
