<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
    Route::get('/', 'PagesController@index');
    Route::get('About', 'PagesController@about');
    Route::get('Contacts', ['as' => 'contact', 'uses' => 'PagesController@contacts']);

    Route::post('Contacts', ['as' => 'contact_store', 'uses' => 'PagesController@store']);
    Route::get('Why-us', 'PagesController@why_us');

    Route::get('Properties', 'PagesController@properties');

    Route::get('edit', 'PagesController@edit');

    Route::get('images/{filename}', 'PropertyController@showImage')->where('filename', '(.*)')->name('images');
    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin')->name('login');
    Route::post('auth/login', 'Auth\AuthController@postLogin')->name('login');


    // Registration routes...
    Route::get('/auth/register', 'Auth\AuthController@getRegister')->name('register');
    Route::post('/auth/register', 'Auth\AuthController@postRegister')->name('register');

    //reset password
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('reset');
    Route::post('password/reset', 'Auth\PasswordController@postReset')->name('reset');

    // Password reset link request routes...
    Route::get('password/email', 'Auth\PasswordController@getEmail')->name('email_password_reset_link');
    Route::post('password/email', 'Auth\PasswordController@postEmail')->name('email_password_reset_link');
});

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/dashboard', 'PagesController@dashboard')->name('dashboard');
    Route::get('/dashboard/contacts', 'PagesController@viewContacts')->name('dashboard_contacts');
    Route::get('/dashboard/contacts/delete/{id}', 'PagesController@destroy')->name('dashboard_contacts_delete');
    Route::resource('/directors', 'DirectorController');
    Route::resource('/properties', 'PropertyController');
    Route::resource('/company', 'CompanyController');
    Route::resource('/carousel', 'CarouselController');
    Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('logout');

});
