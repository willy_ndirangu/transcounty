<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Property;
use App\Http\Requests\PropertyFormRequest;
use App\Repository\PropertyRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class PropertyController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()

    {
        return view('dashboard.properties.show')->with('properties', Property::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.properties.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PropertyFormRequest $request)
    {
        $propertyRepository = new PropertyRepository();
        $propertyRepository->store($request);
        return \Redirect::route('properties.index')->with('message', 'added property!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);
        return view('dashboard.properties.view')->with(['property' => $property]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $property = Property::findOrFail($id);
        return view('dashboard.properties.edit')->with(['property' => $property]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(PropertyFormRequest $request, $id)
    {
        $propertyRepository = new PropertyRepository();
        $propertyRepository->update($request,$id);
        return \Redirect::route('properties.index')->with('message', 'edited property!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Property::findOrFail($id)->delete();
        return \Redirect::route('properties.index')->with('message', 'deleted property!');

    }

    public function showImage($filename)
    {
        $path = storage_path() . '/app/lands/' . $filename;
        if (!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

    }
}
