<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CompanyFormRequest;

class CompanyController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $missions= Company::where('title','!=','Values')->get();
        $values= Company::where('title','Values')->get();
        return view('dashboard.company.show')->with(['missions' => $missions,'values'=>$values]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.company.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CompanyFormRequest $request)
    {
        Company::create($request->all());
        return \Redirect::route('company.index')->with('message', 'added company data!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        return view('dashboard.company.edit')->with(['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(CompanyFormRequest $request, $id)
    {
        Company::findOrFail($id)->update($request->all());
        return \Redirect::route('company.index')->with('message', 'updated director!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Company::findOrFail($id)->delete();
        return \Redirect::route('company.index')->with('message', 'deleted director!');

    }


}
