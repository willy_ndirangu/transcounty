<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CarouselFormRequest;
use App\Carousel;
use App\Repository\CarouselRepository;

class CarouselController extends Controller
{
    public $carouselRepository;

    public function __construct()
    {
        $this->carouselRepository = new CarouselRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.carousel.show')->with(['carousels' => Carousel::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.carousel.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CarouselFormRequest $request)
    {

        $this->carouselRepository->store($request);
        return \Redirect::route('carousel.index')->with('message', 'added carousel!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $carousel = Carousel::findOrFail($id);
        return view('dashboard.carousel.edit')->with(['carousel' => $carousel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(CarouselFormRequest $request, $id)
    {

        $this->carouselRepository->update($request,$id);
        return \Redirect::route('carousel.index')->with('message', 'updated carousel image!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Carousel::findOrFail($id)->delete();
        return \Redirect::route('carousel.index')->with('message', 'deleted carousel image!');

    }
    //
}
