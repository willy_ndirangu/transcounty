<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Director;
use App\Http\Requests\DirectorsFormRequest;

class DirectorController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.directors.show')->with(['directors' => Director::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.directors.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(DirectorsFormRequest $request)
    {
        Director::create($request->all());
        return \Redirect::route('directors.index')->with('message', 'added director!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Director::findOrFail($id);
        return view('dashboard.directors.edit')->with(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(DirectorsFormRequest $request, $id)
    {
        Director::findOrFail($id)->update($request->all());
        return \Redirect::route('directors.index')->with('message', 'updated director!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Director::findOrFail($id)->delete();
        return \Redirect::route('directors.index')->with('message', 'deleted director!');

    }

}
