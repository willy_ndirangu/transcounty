<?php
/*
|--------------------------------------------------------------------------
| Controller File
|--------------------------------------------------------------------------
|
| Here is where the functions to return view for the different pages are
| declared and instantiated.
| index returns the home page
|
*/
namespace App\Http\Controllers;

use App\Carousel;
use Illuminate\Http\Request;

use App\Http\Requests\ContactFormRequest;
use App\Http\Controllers\Controller;
use App\Company;
use App\why_us;
use App\Contact;
use DateTime;
use Barryvdh\DomPDF\PDF as PDF;
use App\Property;
use App\Director;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    #returns the home
    public function index()
    {
        $company_details = Company::get();
        $missions = Company::where('title','!=',"Values")->get();
        $carousels = Carousel::where('title','!=',"Values")->get();

        $values = Company::orderBy('id', 'desc')->take(5)->get();
        return view('pages.Home', compact('company_details', 'values', 'missions','carousels'));
    }

    #returns about view
    public function about()
    {
        return view('pages.About-us')->with('directors', Director::all());
    }

    #returns contacts view
    public function contacts()
    {
        return view('pages.contacts');
    }

    #storing user generated Data in contact form
    public function store(ContactFormRequest $request)
    {
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'message' => $request->get('message'),
            'mailing address' => $request->get('mailing'),
            'subject' => $request->get('subject'),
            'phone_number' => $request->get('phone_number'),

            'created_at' => new DateTime()
        ];
        Contact::insert($data);
        return \Redirect::route('contact')->with('message', 'Thanks for contacting us!');
    }

    #returns why-us view
    public function why_us()
    {
        $reasons = why_us::get();
        return view('pages.why-us', compact('reasons'));
    }

    #returns properties view
    public function properties()
    {
        return view('pages.Properties')->with('properties', Property::all());
    }

    public function viewContacts()
    {
        return view('dashboard.contacts.show')->with('messages', Contact::orderBy('-id')->get());
    }

    public function dashboard()
    {
        return view('dashboard.home.landing');
    }

    public function destroy($id)
    {
        Contact::findOrFail($id)->delete();
        return \Redirect::route('dashboard_contacts')->with('message', 'Deleted message!');
    }

    public function dashboardLanding()
    {
        return view('dashboard.company.show');
    }


}
